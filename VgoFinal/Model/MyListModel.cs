﻿using System;
namespace VgoFinal.Model
{
    public class MyListModel
    {
        public string Name { get; set; }
        public string Detail { get; set; }
        public string Image { get; set; }
        public string Ingredients { get; set; }
        public string Price { get; set; }
        public string PhpPrice => $"PHP {Price}";
    }
}
