﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace VgoFinal.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ProfileTabPage : TabbedPage
    {
        public ProfileTabPage()
        {
            InitializeComponent();
        }
    }
}
